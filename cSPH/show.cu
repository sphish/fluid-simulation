//SPH CUDA&挂链优化&定期排序优化
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <vector>
#include <map>

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
 
#pragma comment (lib, "glfw3.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glew32s.lib")
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "myGL.h"

//#include "MC.cpp"

//*****************************************************************************************8
//*****************************************************************************************8
namespace CUDA_SET {
	void start() {
		cudaError_t cudaStatus;
		cudaStatus = cudaSetDevice(0);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		}
	}
	void over() {
		cudaError_t cudaStatus;
		cudaStatus = cudaDeviceReset();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceReset failed!");
		}
	}
};
#define MC_MAX_TRIANGLES 1000000 //设定最多三角片数目(仅用于调节数组大小，该爆数组还是会爆)
#define MC_MAX_DIVIDE 200 //设定最大分割块数(同上)
#define siearch(a,b,c) ((a)*ys*zs*4+(b)*zs*2+(c))
#define search(a,b,c) ((a)*ys*zs+(b)*zs+(c))
 
//----vector&function for CUDA---------------------------------------
struct mvec3 {
	float h[3];
	__host__ __device__ mvec3(const float &a = 0.f, const  float &b = 0.f, const  float &c = 0.f) {
		h[0] = a;
		h[1] = b;
		h[2] = c;
	}
	__host__ __device__ mvec3 operator + (const mvec3 &t) const {
		return mvec3(h[0] + t.h[0], h[1] + t.h[1], h[2] + t.h[2]);
	}
	__host__ __device__ mvec3 operator - (const mvec3 &t) const {
		return mvec3(h[0] - t.h[0], h[1] - t.h[1], h[2] - t.h[2]);
	}
	__host__ __device__ mvec3 operator * (const float &t) const {
		return mvec3(t*h[0], t*h[1], t*h[2]);
	}
	__host__ __device__ mvec3 operator / (const float &t) const {
		return mvec3(h[0] / t, h[1] / t, h[2] / t);
	}
	__host__ __device__ mvec3 operator += (const mvec3 &t) {
		h[0] += t.h[0];
		h[1] += t.h[1];
		h[2] += t.h[2];
		return *this;
	}
	__host__ __device__ mvec3 operator *= (const float &t) {
		h[0] *= t;
		h[1] *= t;
		h[2] *= t;
		return *this;
	}
};
__host__ __device__ mvec3 operator * (const float &a, const mvec3 &b) {
	return mvec3(a*b.h[0], a*b.h[1], a*b.h[2]);
}
__host__ __device__ float mlength(const mvec3 &t) {
	return sqrt(t.h[0] * t.h[0] + t.h[1] * t.h[1] + t.h[2] * t.h[2]);
}
__host__ __device__ mvec3 mnormalize(const mvec3 &t) {
	return t / mlength(t);
}
template<class T>
__host__ __device__ T cmax(const T &a, const T &b) { return a > b ? a : b; }
template<class T>
__host__ __device__ T cmin(const T &a, const T &b) { return a < b ? a : b; }
struct mivec3 {
	int h[3];
	__host__ __device__ mivec3(int x = 0, int y = 0, int z = 0) {
		h[0] = x;
		h[1] = y;
		h[2] = z;
	}
};


namespace CUDA_MARCHING_CUBE { 
	__global__ void calcValueKernel1(float *fk, int *glist, int *plist, int divi, mvec3 *data, int n, float r, float s, float xl, float yl, float zl, float xr, float yr, float zr, int xs, int ys, int zs) {
		int lim = r / s;
		int gid = blockIdx.x * blockDim.x + threadIdx.x;
		int top = xs * ys * zs, off = blockDim.x * gridDim.x;
		int i, j, k;
		for (int ss = gid; ss < top; ss += off) {
			i = (int)(ss / (ys*zs));
			j = (int)(ss % (ys*zs) / (zs));
			k = ss % (zs);
			mvec3 gg(xl + s*i, yl + s*j, zl + s*k);
			float val = 0;
			if (i >= 1 && i < xs - 2 && j >= 1 && j < ys - 2 && k >= 1 && k < zs - 2) {
				int	xxl = cmax((int)((gg.h[0] - r - xl) / (xr - xl)*divi), 0), xxr = cmin((int)((gg.h[0] + r - xl) / (xr - xl)*divi), divi - 1),
					yyl = cmax((int)((gg.h[1] - r - yl) / (yr - yl)*divi), 0), yyr = cmin((int)((gg.h[1] + r - yl) / (yr - yl)*divi), divi - 1),
					zzl = cmax((int)((gg.h[2] - r - zl) / (zr - zl)*divi), 0), zzr = cmin((int)((gg.h[2] + r - zl) / (zr - zl)*divi), divi - 1);
				for (int ux = xxl; ux <= xxr; ++ux)
					for (int uy = yyl; uy <= yyr; ++uy)
						for (int uz = zzl; uz <= zzr; ++uz) {
							for (int t = glist[ux*divi*divi + uy*divi + uz]; t != -1; t = plist[t]) {
								mvec3 tp = data[t];
								float cnm = r - mlength(gg - tp) + 1;
								int gi = search(i, j, k);
								val = cmax(val, cnm);
							}
						}
				}
			fk[search(i, j, k)] = val;
		}
	}
	__global__ void calcValueKernel(float *fk, mvec3 *data, int n, float r, float s, float xl, float yl, float zl, int xs, int ys, int zs) {
		int lim = r / s;
		int off = blockDim.x * gridDim.x;
		for (int t = blockDim.x * blockIdx.x + threadIdx.x; t < n; t += off) {
			mvec3 tp = data[t];
			int gx = (tp.h[0] - xl) / s, gy = (tp.h[1] - yl) / s, gz = (tp.h[2] - zl) / s;
			int xxl = cmax(1, gx - lim), xxr = cmin(xs - 3, gx + lim),
				yyl = cmax(1, gy - lim), yyr = cmin(ys - 3, gy + lim),
				zzl = cmax(1, gz - lim), zzr = cmin(zs - 3, gz + lim);
			for (int i = xxl; i <= xxr; ++i) {
				for (int j = yyl; j <= yyr; ++j) {
					for (int k = zzl; k <= zzr; ++k) {
						mvec3 hh(xl + s*i, yl + s*j, zl + s*k);
						float cnm = r - mlength(hh - tp) + 1;
						int gi = search(i, j, k);
						if (fk[gi] < cnm)
							fk[gi] = cnm;
						//						fk[gi] = cmax(fk[gi], cnm);
					}
				}
			}
		}
	}
	texture<float> calcPoints_fk;
	extern "C"
		__global__ void calcPointsKernel(mvec3 *pos, int xs, int ys, int zs, float s, float xl, float yl, float zl) {
		int gid = blockIdx.x * blockDim.x + threadIdx.x;
		int top = xs * ys * zs, off = blockDim.x * gridDim.x;
		int i, j, k;
		for (int ss = gid; ss < top; ss += off) {
			i = (int)(ss / (ys*zs));
			j = (int)(ss % (ys*zs) / (zs));
			k = ss % (zs);
			mvec3 gg(xl + s*i, yl + s*j, zl + s*k);
			pos[siearch(2 * i, 2 * j, 2 * k)] = gg;
			float valijk = tex1Dfetch(calcPoints_fk, search(i, j, k));
			pos[siearch(2 * i + 1, 2 * j, 2 * k)] = gg + mvec3(s, 0, 0) * ((1.0f - valijk) / (tex1Dfetch(calcPoints_fk, search((i + 1), j, k)) - valijk));
			pos[siearch(2 * i, 2 * j + 1, 2 * k)] = gg + mvec3(0, s, 0) * ((1.0f - valijk) / (tex1Dfetch(calcPoints_fk, search(i, (j + 1), k)) - valijk));
			pos[siearch(2 * i, 2 * j, 2 * k + 1)] = gg + mvec3(0, 0, s) * ((1.0f - valijk) / (tex1Dfetch(calcPoints_fk, search(i, j, (k + 1))) - valijk));
			pos[siearch(2 * i + 1, 2 * j + 1, 2 * k + 1)] = gg;
		}
	}

	const int triTable[256][16] = { { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1 },
	{ 8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1 },
	{ 3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1 },
	{ 4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1 },
	{ 4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1 },
	{ 9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1 },
	{ 10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1 },
	{ 5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1 },
	{ 5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1 },
	{ 8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1 },
	{ 2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1 },
	{ 2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1 },
	{ 11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1 },
	{ 5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1 },
	{ 11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1 },
	{ 11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1 },
	{ 2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1 },
	{ 6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1 },
	{ 3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1 },
	{ 6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1 },
	{ 6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1 },
	{ 8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1 },
	{ 7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1 },
	{ 3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1 },
	{ 0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1 },
	{ 9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1 },
	{ 8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1 },
	{ 5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1 },
	{ 0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1 },
	{ 6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1 },
	{ 10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1 },
	{ 1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1 },
	{ 0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1 },
	{ 3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1 },
	{ 6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1 },
	{ 9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1 },
	{ 8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1 },
	{ 3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1 },
	{ 10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1 },
	{ 10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1 },
	{ 2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1 },
	{ 7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1 },
	{ 2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1 },
	{ 1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1 },
	{ 11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1 },
	{ 8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1 },
	{ 0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1 },
	{ 7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1 },
	{ 7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1 },
	{ 10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1 },
	{ 0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1 },
	{ 7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1 },
	{ 6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1 },
	{ 4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1 },
	{ 10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1 },
	{ 8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1 },
	{ 1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1 },
	{ 10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1 },
	{ 10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1 },
	{ 9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1 },
	{ 7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1 },
	{ 3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1 },
	{ 7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1 },
	{ 3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1 },
	{ 6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1 },
	{ 9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1 },
	{ 1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1 },
	{ 4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1 },
	{ 7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1 },
	{ 6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1 },
	{ 0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1 },
	{ 6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1 },
	{ 0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1 },
	{ 11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1 },
	{ 6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1 },
	{ 5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1 },
	{ 9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1 },
	{ 1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1 },
	{ 10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1 },
	{ 0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1 },
	{ 11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1 },
	{ 9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1 },
	{ 7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1 },
	{ 2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1 },
	{ 9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1 },
	{ 9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1 },
	{ 1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1 },
	{ 0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1 },
	{ 10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1 },
	{ 2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1 },
	{ 0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1 },
	{ 0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1 },
	{ 9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1 },
	{ 5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1 },
	{ 5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1 },
	{ 8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1 },
	{ 9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1 },
	{ 1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1 },
	{ 3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1 },
	{ 4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1 },
	{ 9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1 },
	{ 11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1 },
	{ 11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1 },
	{ 2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1 },
	{ 9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1 },
	{ 3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1 },
	{ 1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1 },
	{ 4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1 },
	{ 0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1 },
	{ 9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1 },
	{ 1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
	{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 } };;

	float fk[MC_MAX_DIVIDE*MC_MAX_DIVIDE*MC_MAX_DIVIDE];//每个点的等值
	//std::map<int, glm::vec3> pos;//对应点的法向量，由于32位系统内存不能太多只能这么压一下内存
	std::map<int, glm::vec3> norm;//对应点的具体坐标

	int forret[3 * MC_MAX_TRIANGLES];//模型的对应点列
	float forreturn[2 * 3 * 3 * MC_MAX_TRIANGLES];//模型

	int n, xs, ys, zs;
	float *data, r, s, xl, xr, yl, yr, zl, zr;
	int stop, top;

	const int divi = 10;
	int glist[divi*divi*divi];
	int plist[106666];//粒子的数目 
	void calcGlist() {
		memset(plist, 0, sizeof(plist));
		for (int i = 0; i < divi*divi*divi; ++i)
			glist[i] = -1;

		for (int i = 0; i < n; ++i) {
			mvec3 g = mvec3(data[3*i], data[3*i+1], data[3*i+2]);
			int u = (g.h[0] - xl) / (xr - xl) * divi;
			int v = (g.h[1] - yl) / (yr - yl) * divi;
			int w = (g.h[2] - zl) / (zr - zl) * divi;
			plist[i] = glist[u*divi*divi + v*divi + w];
			glist[u*divi*divi + v*divi + w] = i;
		}
	}
	void CUDAcalcValue() {
		calcGlist();
		cudaError_t cudaStatus;

		float *_fk = 0;
		int *_glist = 0;
		int *_plist = 0;
		mvec3 *_data = 0;
		cudaStatus = cudaMalloc((void**)&_fk, xs*ys*zs * sizeof(float));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Malloc failed 060.\n");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&_glist, sizeof(glist));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Malloc failed 0601.\n");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&_plist, n*sizeof(int));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Malloc failed 0602.\n");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&_data, n * sizeof(mvec3));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Malloc failed 061.\n");
			goto Error;
		}
		cudaStatus = cudaMemcpy(_data, data, n * sizeof(mvec3), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Memcpy failed 062.\n");
			goto Error;
		}
		cudaStatus = cudaMemcpy(_glist, glist, sizeof(glist), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Memcpy failed 0620.\n");
			goto Error;
		}
		cudaStatus = cudaMemcpy(_plist, plist, n*sizeof(int), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Memcpy failed 0621.\n");
			goto Error;
		}
		int vdim = 512;
		calcValueKernel << <vdim, vdim >> > (_fk, _data, n, r, s, xl, yl, zl, xs, ys, zs);
		//calcValueKernel1 << <vdim, vdim >> > (_fk, _glist, _plist, divi, _data, n, r, s, xl, yl, zl, xr, yr, zr, xs, ys, zs);
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "addKernel launch failed 063: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel! 064\n", cudaStatus);
			goto Error;
		}
		cudaStatus = cudaMemcpy(fk, _fk, xs*ys*zs * sizeof(float), cudaMemcpyDeviceToHost);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "CUDA Memcpy failed 065.\n");
			goto Error;
		}
	Error:
		cudaFree(_fk);
		cudaFree(_glist);
		cudaFree(_plist);
		cudaFree(_data);
	}
	void calcValue() {
		int lim = r / s;
		for (int t = 0; t < n; ++t) {
			glm::vec3 tp(data[3 * t], data[3 * t + 1], data[3 * t + 2]);
			int gx = (tp.x - xl) / s, gy = (tp.y - yl) / s, gz = (tp.z - zl) / s;
			for (int i = std::max(1, gx - lim); i <= std::min(xs - 3, gx + lim); ++i) {
				for (int j = std::max(1, gy - lim); j <= std::min(ys - 3, gy + lim); ++j) {
					for (int k = std::max(1, gz - lim); k <= std::min(zs - 3, gz + lim); ++k) {
						glm::vec3 hh(xl + s*i, yl + s*j, zl + s*k);
						float cnm = r - glm::length(hh - tp) + 1;
						fk[search(i, j, k)] = std::max(fk[search(i, j, k)], cnm);
					}
				}
			}
		}
		return;
	}
	//extern "C"
	//	void calcPoints() {
	//	//需要xs,ys,zs,s,xl,yl,zl,fk
	//	//导出到pos数组
	//	cudaError_t cudaStatus;

	//	float *_fk = 0;
	//	mvec3 *_pos = 0;
	//	cudaStatus = cudaMalloc((void**)&_fk, xs*ys*zs * sizeof(float));
	//	if (cudaStatus != CUDA_SUCCESS) {
	//		fprintf(stderr, "CUDA Malloc failed 010.\n");
	//		goto Error;
	//	}
	//	cudaStatus = cudaMalloc((void**)&_pos, xs*ys*zs * 8 * 3 * sizeof(float));
	//	if (cudaStatus != CUDA_SUCCESS) {
	//		fprintf(stderr, "CUDA Malloc failed 011.\n");
	//		goto Error;
	//	}
	//	cudaStatus = cudaMemcpy(_fk, fk, xs*ys*zs * sizeof(float), cudaMemcpyHostToDevice);
	//	if (cudaStatus != CUDA_SUCCESS) {
	//		fprintf(stderr, "CUDA Memcpy failed 012.\n");
	//		goto Error;
	//	}
	//	cudaStatus = cudaBindTexture(0, calcPoints_fk, _fk);
	//	if (cudaStatus != CUDA_SUCCESS) {
	//		fprintf(stderr, "CUDA Bind Texture failed 0121.\n");
	//		goto Error;
	//	}
	//	int vdim = 512; 
	//	calcPointsKernel << <vdim, vdim >> > (_pos, xs, ys, zs, s, xl, yl, zl);
	//	cudaStatus = cudaGetLastError();
	//	if (cudaStatus != cudaSuccess) {
	//		fprintf(stderr, "addKernel launch failed 013: %s\n", cudaGetErrorString(cudaStatus));
	//		goto Error;
	//	}
	//	cudaStatus = cudaDeviceSynchronize();
	//	if (cudaStatus != cudaSuccess) {
	//		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel! 014\n", cudaStatus);
	//		goto Error;
	//	}

	//	cudaStatus = cudaMemcpy(pos, _pos, xs*ys*zs * 8 * 3 * sizeof(float), cudaMemcpyDeviceToHost);
	//	if (cudaStatus != CUDA_SUCCESS) {
	//		fprintf(stderr, "CUDA Memcpy failed 015.\n");
	//		goto Error;
	//	}

	//Error:
	//	cudaUnbindTexture(calcPoints_fk);
	//	cudaFree(_fk);
	//	cudaFree(_pos);


	//	pos[siearch(2 * xs - 2, 2 * ys - 2, 2 * zs - 2)] = glm::vec3(xl + s*xs - s, yl + s*ys - s, zl + s*zs - s);
	//}
	glm::vec3 calconePoint(int tt) {

		int yz = ys*zs, zz = zs, yz4 = ys*zs * 4, zz2 = zs * 2;

		int gi = tt / yz4,
			gj = (tt - gi * yz4) / zz2,
			gk = tt % zz2;

		int i = gi >> 1, j = gj >> 1, k = gk >> 1;
		glm::vec3 gg(xl + s*i, yl + s*j, zl + s*k);
		int huaji = i*yz + j*zz + k;

		float valijk = fk[huaji], cnm = 1.f - valijk;
		if (gi & 1)	return  gg + glm::vec3(s*(cnm / (fk[huaji + yz] - valijk)), 0, 0);
		if (gj & 1) return  gg + glm::vec3(0, s*(cnm / (fk[huaji + zz] - valijk)), 0);
		if (gk & 1) return  gg + glm::vec3(0, 0, s*(cnm / (fk[huaji +  1] - valijk)));
		return  gg;
	}
	//void normcalcPoints() {
	//	int yz = ys*zs, zz = zs, yz4 = ys*zs * 4, zz2 = zs * 2;
	//	for (int i = 0; i < xs-1; ++i)
	//		for (int j = 0; j < ys-1; ++j)
	//			for (int k = 0; k < zs-1; ++k) {
	//				glm::vec3 gg(xl + s*i, yl + s*j, zl + s*k);
	//				int huaji = i*yz + j*zz + k, huaji2 = 2 * i*yz4 + 2 * j*zz2 + 2 * k;
	//				pos[huaji2 + 1] = pos[huaji2 + zz2] = pos[huaji2 + yz4] = pos[huaji2] = gg;
	//				float valijk = fk[huaji], cnm = 1.f - valijk;
	//				pos[huaji2 + yz4].x += s*(cnm / (fk[huaji + yz] - valijk));
	//				pos[huaji2 + zz2].y += s*(cnm / (fk[huaji + zz] - valijk));
	//				pos[huaji2 +   1].z += s*(cnm / (fk[huaji +  1] - valijk));
	//			}
	//}
	void calcMesh() {
		for (int i = 1; i < xs - 1; ++i)
			for (int j = 1; j < ys - 1; ++j)
				for (int k = 1; k < zs - 1; ++k) {
					int huaji = 0;
					if (fk[search(i - 1, j - 1, k - 1)] > 1) huaji |= 1;
					if (fk[search(i, j - 1, k - 1)] > 1) huaji |= 2;
					if (fk[search(i, j - 1, k)] > 1) huaji |= 4;
					if (fk[search(i - 1, j - 1, k)] > 1) huaji |= 8;
					if (fk[search(i - 1, j, k - 1)] > 1) huaji |= 16;
					if (fk[search(i, j, k - 1)] > 1) huaji |= 32;
					if (fk[search(i, j, k)] > 1) huaji |= 64;
					if (fk[search(i - 1, j, k)] > 1) huaji |= 128;
					for (int g = 0; ; ++g) {
						int cho = triTable[huaji][g];
						if (cho < 0) break;
						glm::ivec3 haha;
						switch (cho) {
						case 0: {
							haha = glm::ivec3(2 * i - 1, 2 * j - 2, 2 * k - 2);
							break;
						}
						case 2: {
							haha = glm::ivec3(2 * i - 1, 2 * j - 2, 2 * k);
							break;
						}
						case 4: {
							haha = glm::ivec3(2 * i - 1, 2 * j, 2 * k - 2);
							break;
						}
						case 6: {
							haha = glm::ivec3(2 * i - 1, 2 * j, 2 * k);
							break;
						}
						case 8: {
							haha = glm::ivec3(2 * i - 2, 2 * j - 1, 2 * k - 2);
							break;
						}
						case 9: {
							haha = glm::ivec3(2 * i, 2 * j - 1, 2 * k - 2);
							break;
						}
						case 10: {
							haha = glm::ivec3(2 * i, 2 * j - 1, 2 * k);
							break;
						}
						case 11: {
							haha = glm::ivec3(2 * i - 2, 2 * j - 1, 2 * k);
							break;
						}
						case 1: {
							haha = glm::ivec3(2 * i, 2 * j - 2, 2 * k - 1);
							break;
						}
						case 3: {
							haha = glm::ivec3(2 * i - 2, 2 * j - 2, 2 * k - 1);
							break;
						}
						case 5: {
							haha = glm::ivec3(2 * i, 2 * j, 2 * k - 1);
							break;
						}
						case 7: {
							haha = glm::ivec3(2 * i - 2, 2 * j, 2 * k - 1);
							break;
						}
						default: {
							printf("ERROR: Marching Cube 001\n");
							while (1);
							break;
						}
						}
						forret[++stop] = siearch(haha.x, haha.y, haha.z);
					}
				}
	}
		float *run(int &res_int, int _n, float *_data, float _r, float _s, float _xl, float _xr, float _yl, float _yr, float _zl, float _zr) {
		n = _n;
		data = _data;
		r = _r;
		s = _s;
		xl = _xl;
		xr = _xr;
		yl = _yl;
		yr = _yr;
		zl = _zl;
		zr = _zr;

		xs = (xr - xl) / s, ys = (yr - yl) / s, zs = (zr - zl) / s;

		//返回：三角形列(x y z Nx Ny Nz)
		//printf("---1\n");
		memset(fk, 0, sizeof(fk));
		//printf("---2\n");

		//将粒子覆盖体弄出来

		//以1为等值面
		//calcValue();
		CUDAcalcValue();
		 
		//用点的等值算出等值点坐标
		//calcPoints();
		//normcalcPoints();

		//用体素计算模型
		stop = -1, top = -1;
		calcMesh();

		//将模型弄成数组返回
		for (int i = 0; i <= stop; i += 3) {
			int d[3] ={forret[i], forret[i+1], forret[i+2]};
			glm::vec3 normal = glm::normalize(glm::cross(calconePoint(d[1])- calconePoint(d[0]), calconePoint(d[2])- calconePoint(d[0])));
			norm[d[0]] += normal; norm[d[1]] += normal; norm[d[2]] += normal;
		}
		//float forreturn[2 * 3 * 3 * MC_MAX_TRIANGLES];
		for (int i = 0; i <= stop; i += 3) {
			int d[3] = { forret[i], forret[i + 1], forret[i + 2]};
			for (int k = 0; k < 3; ++k) {
				glm::vec3 hulu;
				hulu = calconePoint(d[k]);
				forreturn[++top] = hulu.x;
				forreturn[++top] = hulu.y;
				forreturn[++top] = hulu.z;
				hulu = norm[d[k]];
				forreturn[++top] = hulu.x;
				forreturn[++top] = hulu.y;
				forreturn[++top] = hulu.z;
			}
			if ((top+1) == 18 * MC_MAX_TRIANGLES) {
				printf("ERROR: Marching Cube 002  triangles number %d\n", top / 18);
				while (1);
			}
		}
		if ((top + 1) % 18 != 0) {
			printf("ERROR: Marching Cube 003\n");
			while (1);
		}
		printf("vertices: %d top %d\n", stop + 1, top);

		norm.clear();

		res_int = (top + 1) / 18;
		return forreturn;
		//		return std::pair<int, float*>((top + 1) / 18, forreturn);
	}
};
//*****************************************************************************************8
const GLuint SCREEN_WIDTH = 1024;
const GLuint SCREEN_HEIGHT = 768;
class SkyBox {
public:
	GLuint cubemapTexture;
	Shader BoxShader;
	GLuint skyBoxVAO;
	GLuint loadCubemap(std::vector<const GLchar*> faces) {
		GLuint textureID;
		glGenTextures(1, &textureID);

		int width1, height1;
		unsigned char* image;
		//stbi_load("container.jpg", &width, &height, &nrChannels, 0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
		for (GLuint i = 0; i < faces.size(); i++) {
			image = stbi_load(faces[i], &width1, &height1, 0, 0);
			glTexImage2D(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
				GL_RGB, width1, height1, 0, GL_RGB, GL_UNSIGNED_BYTE, image
			);
			stbi_image_free(image);
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		return textureID;
	}
	void make() {
		std::vector<const GLchar*> faces;

		faces.push_back("./skybox/1right.jpg");
		faces.push_back("./skybox/1left.jpg");
		faces.push_back("./skybox/1top.jpg");
		faces.push_back("./skybox/1bottom.jpg");
		faces.push_back("./skybox/1back.jpg");
		faces.push_back("./skybox/1front.jpg");

		cubemapTexture = loadCubemap(faces);

		GLfloat skyboxVertices[] = {
			// Positions          
			-1.0f,  1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f,  1.0f,
			1.0f,  1.0f,  1.0f,
			1.0f,  1.0f,  1.0f,
			1.0f,  1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			1.0f,  1.0f,  1.0f,
			1.0f,  1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			-1.0f,  1.0f, -1.0f,
			1.0f,  1.0f, -1.0f,
			1.0f,  1.0f,  1.0f,
			1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f
		};
		GLuint skyBoxVBO;
		glGenVertexArrays(1, &skyBoxVAO);
		glGenBuffers(1, &skyBoxVBO);

		glBindVertexArray(skyBoxVAO);
		glBindBuffer(GL_ARRAY_BUFFER, skyBoxVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glBindVertexArray(0);

		BoxShader.mkShader("glshader/skyBoxShader.vert", NULL, "glshader/skyBoxShader.frag");
	}
	void Draw(glm::mat4 viewMat, glm::mat4 persMat) {
		glDepthMask(GL_FALSE);

		BoxShader.Use();

		glUniformMatrix4fv(BoxShader.sUni("viewMat"), 1, GL_FALSE, &viewMat[0][0]);
		glUniformMatrix4fv(BoxShader.sUni("persMat"), 1, GL_FALSE, &persMat[0][0]);

		glBindVertexArray(skyBoxVAO);
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(BoxShader.sUni("skybox"), 0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0 * 3 * sizeof(GLfloat), 36);
		glBindVertexArray(0);

		glDepthMask(GL_TRUE);
	}
};
//输入数据：一个数n表示粒子数目，一个
namespace fuck {
	const mvec3 initialVelocity = mvec3(1.8, -1.8, 0);
	struct Particle {
		mvec3 position;
		mvec3 velocity;
		mvec3 force;
		float density;
		float pressure;
		__host__ __device__ Particle()
		{
			position = force = mvec3(0, 0, 0);
			density = pressure = 0.f;
			velocity = mvec3(1.8, -1.8, 0);// initialVelocity;
		}
	};
	//----------------------CUDA--------------------------
	__device__ float CUDA_kernelPoly6(mvec3 d, float h)
	{
		float r = mlength(d);
		return pow(h * h - r * r, 3);
	}
	texture<float> texparticle;
	__global__ void CUDA_computeDensityPressure(Particle *particles, int *_next, int *_lis, int divi, int particleCount, float smoothingRadius, float particleMass, float kPoly6, float stiffness, float restDensity)
	{
		int off = blockDim.x * gridDim.x;
		for (int particleIndex = blockDim.x*blockIdx.x + threadIdx.x; particleIndex < particleCount; particleIndex += off) {
			mvec3 positionX = particles[particleIndex].position;
			// compute density
			float densitySum = 0.f;
			//遍历相邻方格的粒子
			int xxl = cmax(0, (int)((positionX.h[0]- smoothingRadius + 1.f) / 2.f * divi)), xxr = cmin((int)((positionX.h[0] + smoothingRadius + 1.f) / 2.f * divi), divi),
				yyl = cmax(0, (int)((positionX.h[1]- smoothingRadius + 1.f) / 2.f * divi)), yyr = cmin((int)((positionX.h[1] + smoothingRadius + 1.f) / 2.f * divi), divi),
				zzl = cmax(0, (int)((positionX.h[2]- smoothingRadius + 1.f) / 2.f * divi)), zzr = cmin((int)((positionX.h[2] + smoothingRadius + 1.f) / 2.f * divi), divi);
			for (int si = xxl; si <= xxr; ++si)
				for (int sj = yyl; sj <= yyr; ++sj)
					for (int sk = zzl; sk <= zzr; ++sk) {
						for (int gg = _lis[si*divi*divi + sj*divi + sk]; gg != -1; gg = _next[gg]) {
							/*mvec3 epos = mvec3(tex1Dfetch(texparticle, 11 * gg),
								tex1Dfetch(texparticle, 11 * gg + 1),
								tex1Dfetch(texparticle, 11 * gg + 2)
							);*/
							mvec3 epos = particles[gg].position;
							mvec3 positionI = epos;
							mvec3 positionDelta = positionX - positionI;
							if (mlength(positionDelta) < smoothingRadius)
								densitySum += CUDA_kernelPoly6(positionDelta, smoothingRadius);
						}
					}
			particles[particleIndex].density = densitySum * particleMass * kPoly6;
			particles[particleIndex].pressure = stiffness * (particles[particleIndex].density - restDensity);
		}
	}
	__device__ float CUDA_rand() {
		float gg;
		return gg;
	}
	__device__ mvec3 CUDA_kernelSpiky(mvec3 d, float h)
	{
		float r = mlength(d);
		if (r < 1e-7)
			return mnormalize(mvec3(CUDA_rand(), CUDA_rand(), CUDA_rand()));
		return mnormalize(d) * pow(h - r, 2);
	}
	__device__ float CUDA_kernelViscosity(mvec3 o, float h) {
		float r = mlength(o);
		return (h - r);
	}
	__device__ mvec3 CUDA_kernelDeltaCs(mvec3 o, float h) {
		float r = mlength(o);
		return o * pow(h * h - r * r, 2);
	}
	__device__ float CUDA_kernelDeltaQcs(mvec3 o, float h) {
		float r = mlength(o);
		return (h * h - r * r) * (r * r - 3.f / 4.f * (h * h - r * r));
	}
	__global__ void CUDA_computeForce(Particle *particles, int *_next, int *_lis, int divi, int particleCount, float smoothingRadius, float particleMass, float kSpiky, float kDeltaCs, float kDeltaQcs, float viscosity, float kViscosity, float gravity, float surfaceTension)
	{
		int off = blockDim.x * gridDim.x;
		Particle myself;
		for (int particleIndex = blockDim.x*blockIdx.x + threadIdx.x; particleIndex < particleCount; particleIndex += off) {
			//myself = particles[particleIndex];
			int gili = 11*particleIndex;
			myself.position = mvec3(tex1Dfetch(texparticle, gili),
									tex1Dfetch(texparticle, gili+1),
									tex1Dfetch(texparticle, gili+2));
			myself.velocity = mvec3(tex1Dfetch(texparticle, gili+3),
				tex1Dfetch(texparticle, gili + 4),
				tex1Dfetch(texparticle, gili + 5));
			myself.density = tex1Dfetch(texparticle, gili + 9);
			myself.pressure = tex1Dfetch(texparticle, gili + 10);

			mvec3 positionX = myself.position;
			float densityX = myself.density;
			float pressureX = myself.pressure;
			mvec3 velocityX = myself.velocity;

			mvec3 pressureForce = mvec3(0, 0, 0);
			mvec3 viscosityForce = mvec3(0, 0, 0);
			mvec3 surfaceTensionForce = mvec3(0, 0, 0);
			mvec3 deltaCs = mvec3(0, 0, 0);
			float deltaQcs = 0;

			int xxl = cmax(0, (int)((positionX.h[0] - smoothingRadius + 1.f) / 2.f * divi)), xxr = cmin((int)((positionX.h[0] + smoothingRadius + 1.f) / 2.f * divi), divi),
				yyl = cmax(0, (int)((positionX.h[1] - smoothingRadius + 1.f) / 2.f * divi)), yyr = cmin((int)((positionX.h[1] + smoothingRadius + 1.f) / 2.f * divi), divi),
				zzl = cmax(0, (int)((positionX.h[2] - smoothingRadius + 1.f) / 2.f * divi)), zzr = cmin((int)((positionX.h[2] + smoothingRadius + 1.f) / 2.f * divi), divi);
			for (int si = xxl; si <= xxr; ++si)
				for (int sj = yyl; sj <= yyr; ++sj)
					for (int sk = zzl; sk <= zzr; ++sk) {
						for (int gg = _lis[si*divi*divi + sj*divi + sk]; gg != -1; gg = _next[gg]) {
							if (particleIndex == gg)
								continue;
							//Particle e = particles[gg];
							Particle e;
							gili = 11 * gg;
							e.position = mvec3(tex1Dfetch(texparticle, gili),
								tex1Dfetch(texparticle, gili + 1),
								tex1Dfetch(texparticle, gili + 2));
							e.velocity = mvec3(tex1Dfetch(texparticle, gili + 3),
								tex1Dfetch(texparticle, gili + 4),
								tex1Dfetch(texparticle, gili + 5));
							e.density = tex1Dfetch(texparticle, gili + 9);
							e.pressure = tex1Dfetch(texparticle, gili + 10);

							mvec3 positionI = e.position;
							float densityI = e.density;
							float pressureI = e.pressure;
							mvec3 velocityI = e.velocity;
							mvec3 positionDelta = positionX - positionI;
							if (mlength(positionDelta) < smoothingRadius) {
								pressureForce += -particleMass * (pressureX + pressureI) / (2.f * densityI) * CUDA_kernelSpiky(positionDelta, smoothingRadius);
								viscosityForce += particleMass * (velocityI - velocityX) / densityI * CUDA_kernelViscosity(positionDelta, smoothingRadius);
								deltaCs += particleMass / densityI * CUDA_kernelDeltaCs(positionDelta, smoothingRadius);
								deltaQcs += particleMass / densityI * CUDA_kernelDeltaQcs(positionDelta, smoothingRadius);
							}
						}
					}

			pressureForce *= kSpiky;
			deltaCs *= kDeltaCs;
			deltaQcs *= kDeltaQcs;
			mvec3 externalForce = densityX * mvec3(0, -gravity, 0);
			viscosityForce *= viscosity * kViscosity;
			if (deltaQcs > 1)
				surfaceTensionForce = -surfaceTension * deltaQcs / densityX * mnormalize(deltaCs);

			particles[particleIndex].force = pressureForce + viscosityForce + externalForce + surfaceTensionForce;
		}
	}
	__global__ void CUDA_updateVelocityPosition(Particle *particles, int particleCount, float timeInterval, float restitutionCoefficient)
	{
		int off = blockDim.x * gridDim.x;
		Particle myself;
		for (int particleIndex = blockDim.x*blockIdx.x + threadIdx.x; particleIndex < particleCount; particleIndex += off) {
			// update velocity and position
			myself = particles[particleIndex];
			mvec3 positionX = myself.position;
			float densityX = myself.density;
			mvec3 velocityX = myself.velocity;
			mvec3 forceX = myself.force;

			mvec3 _velocity = velocityX + timeInterval * forceX / densityX;
			mvec3 _position = positionX + timeInterval * _velocity;

			// handle boundary collision
			if (_position.h[0] <= -1) {
				_position.h[0] = -0.999;
				_velocity.h[0] *= -restitutionCoefficient;
			}
			else if (_position.h[0] >= 1) {
				_position.h[0] = 0.999;
				_velocity.h[0] *= -restitutionCoefficient;
			}
			if (_position.h[1] <= -1) {
				_position.h[1] = -0.999;
				_velocity.h[1] *= -restitutionCoefficient;
			}
			else if (_position.h[1] >= 1) {
				_position.h[1] = 0.999;
				_velocity.h[1] *= -restitutionCoefficient;
			}
			if (_position.h[2] <= -1) {
				_position.h[2] = -0.999;
				_velocity.h[2] *= -restitutionCoefficient;
			}
			else if (_position.h[2] >= 1) {
				_position.h[2] = 0.999;
				_velocity.h[2] *= -restitutionCoefficient;
			}

			particles[particleIndex].velocity = _velocity;
			particles[particleIndex].position = _position;
		}
	}
	//----------------------------------------------------

	// global info
	const float timeInterval = 0.005f;
	unsigned particleCount = 100000;

	// physical constant 
	const float PI = 3.14159265358979323846f;
	const float viscosity = 3.5;
	const float gravity = 9.8;
	const float restitutionCoefficient = 0.8;

	// particle info


	const float restDensity = 998.91;
	const float particleMass = 0.02;
	const float surfaceTension = 0.0728;
	const float smoothingRadius = 0.0457;

	const float particleLength = 0.0114;
	const int width = 176;
	const float stiffness = 3.0;
	// kernal function constants
	//Poly6 Kernel
	const float kPoly6 = 315.0f / (64.0f * 3.141592f * pow(smoothingRadius, 9));
	//Spiky Kernel
	const float kSpiky = -45.0f / (3.141592f * pow(smoothingRadius, 6));
	//Viscosity Kernel
	const float kViscosity = 45.0f / (3.141592f * pow(smoothingRadius, 6));
	//Surface Tension Kernel
	const float kDeltaCs = -945 / (32.f * PI * pow(smoothingRadius, 9));
	const float kDeltaQcs = 945 / (8.f * PI * pow(smoothingRadius, 9));
	// 以上是计算用到的常量

	Particle particles[width * width * width];

	float forreturn[100111 * 3];
	int next[120000];
	const int divi = 80;
	int lis[85*85*85];

	Particle *_particles = 0;
	float *_particles1 = 0;
	int *_lis = 0;
	int *_next = 0;
	int fraps = 0;
	mivec3 getSQU(mvec3 s) {
		int ui = (s.h[0] - (-1.f)) / (2.f) *divi;
		int uj = (s.h[1] - (-1.f)) / (2.f) *divi;
		int uk = (s.h[2] - (-1.f)) / (2.f) *divi;
		return mivec3(ui, uj, uk);
	}
	bool cmp(const Particle &a, const Particle &b) {
		mivec3 aa = getSQU(a.position), bb = getSQU(b.position);
		return aa.h[0] == bb.h[0] ? (aa.h[2]==bb.h[2]?(aa.h[1]<bb.h[1]):(aa.h[2]<bb.h[2])): (aa.h[0] < bb.h[0]);
	}
	void resort() {
		std::sort(particles, particles+particleCount, cmp);
		cudaError_t cudaStatus = cudaMemcpy(_particles, particles, particleCount * sizeof(Particle), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA resort Memcpy failed. 025\n");
		}
	}
	void initializeParticles(int type) {
		unsigned a = 50, b = 50, c = 40;
		if (type == 1)
			a = 25, b = 40, c = 50;

		particleCount = a * b * c;

		// initial 
		for (auto x = 0; x < a; ++x)
			for (auto y = 0; y < b; ++y)
				for (auto z = 0; z < c; ++z) {
					particles[x * b * c + y * c + z].position = mvec3(particleLength * x * 2 - 1, particleLength * y * 2 - 1, particleLength * z);
				}

		cudaError_t cudaStatus;

		cudaStatus = cudaMalloc((void**)&_particles, particleCount * sizeof(Particle));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Malloc failed. 020\n");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&_particles1, particleCount * sizeof(Particle));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Malloc failed. 0201\n");
			goto Error;
		}

		cudaStatus = cudaMalloc((void**)&_lis, sizeof(lis));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Malloc failed. 0202\n");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&_next, sizeof(next));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Malloc failed. 0203\n");
			goto Error;
		}
		cudaStatus = cudaMemcpy(_particles, particles, particleCount * sizeof(Particle), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Memcpy failed. 021\n");
			goto Error;
		}
		Error:
	}
	void endParticles() {
		cudaFree(_particles);
		cudaFree(_particles1);
		cudaFree(_lis);
		cudaFree(_next);
	}
	void mkList() {
		for (int i = 0; i < 85*85*85; ++i)
			lis[i] = -1;
		for (int i = 0; i < particleCount; ++i) {
			mvec3 s = particles[i].position;
			int ui = (s.h[0] - (-1.f)) / (2.f) *divi;
			int uj = (s.h[1] - (-1.f)) / (2.f) *divi;
			int uk = (s.h[2] - (-1.f)) / (2.f) *divi;
			next[i] = lis[ui*divi*divi + uj*divi + uk];
			lis[ui*divi*divi + uj*divi + uk] = i;
		}
	}
	void CUDA_computeFrame() {
		++fraps;
		if (fraps % 30 == 0)
			resort();
		//定期排序加速CUDA.

		mkList();
		cudaError_t cudaStatus;
		cudaStatus = cudaMemcpy(_lis, lis, sizeof(lis), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) { 
			fprintf(stderr, "ERROR: CUDA Memcpy failed. 0210\n");
			goto Error;
		}
		cudaStatus = cudaMemcpy(_next, next, sizeof(next), cudaMemcpyHostToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Memcpy failed. 0211\n");
			goto Error;
		}
		CUDA_computeDensityPressure << <512, 512 >> > (_particles, _next, _lis, divi, particleCount, smoothingRadius, particleMass, kPoly6, stiffness, restDensity);
		{
			cudaStatus = cudaGetLastError();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "addKernel launch failed 0220: %s\n", cudaGetErrorString(cudaStatus));
				goto Error;
			}
			cudaStatus = cudaDeviceSynchronize();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel! 0221\n", cudaStatus);
				goto Error;
			}
		}
		//CUDA_mktex(_particles, _particlePos, particleCount);
		cudaStatus = cudaMemcpy(_particles1, _particles, particleCount * sizeof(Particle), cudaMemcpyDeviceToDevice);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Memcpy failed. 0212\n");
			goto Error;
		}
		cudaStatus = cudaBindTexture(0, texparticle, _particles1, particleCount * sizeof(Particle));
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Bind texture failed. 0213\n");
			goto Error;
		}
		CUDA_computeForce << <512, 512 >> > (_particles, _next, _lis, divi, particleCount, smoothingRadius, particleMass, kSpiky, kDeltaCs, kDeltaQcs, viscosity, kViscosity, gravity, surfaceTension);
		{
			cudaStatus = cudaGetLastError();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "addKernel launch failed 0230: %s\n", cudaGetErrorString(cudaStatus));
				goto Error;
			}
			cudaStatus = cudaDeviceSynchronize();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel! 0231\n", cudaStatus);
				goto Error;
			}
		}
		CUDA_updateVelocityPosition << <512, 512 >> >(_particles, particleCount, timeInterval, restitutionCoefficient);
		{
			cudaStatus = cudaGetLastError();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "addKernel launch failed 0240: %s\n", cudaGetErrorString(cudaStatus));
				goto Error;
			}
			cudaStatus = cudaDeviceSynchronize();
			if (cudaStatus != cudaSuccess) {
				fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel! 0241\n", cudaStatus);
				goto Error;
			}
		}
		cudaStatus = cudaMemcpy(particles, _particles, particleCount*sizeof(Particle), cudaMemcpyDeviceToHost);
		if (cudaStatus != CUDA_SUCCESS) {
			fprintf(stderr, "ERROR: CUDA Memcpy failed. 025\n");
			goto Error;
		}
	Error:
		cudaUnbindTexture(texparticle);
	}
};
class VERTEXARRAYOBJECT {
public:
	virtual void make() {}
	virtual void draw() {}
};
class QIU : public VERTEXARRAYOBJECT {
private:
	GLuint VAO, VBO;
	int stop;
	glm::vec3 calc(int i, int j, int div) {
		//[0,12)
		float u = 1.0f * i * 3.14159f * 2 / div;
		float v = 1.0f * j * 3.14159f / div - 1.570796f;
		return glm::normalize(glm::vec3(cos(u), tan(v), -sin(u)));
	}
public:
	virtual void make() {
		//画一个球
		int div = 80;
		GLfloat *ball = new GLfloat[div*div * 2 * 3 * 6];
		int btop = -1;
		for (int i = 0; i < div; ++i)
			for (int j = 0; j < div; ++j) {
				//(i,j)->(i+1,j)->(i+1,j+1)
				glm::vec3 sb;
				sb = calc(i, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				//(i,j)->(i+1,j+1)->(i,j+1)
				sb = calc(i, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
			}

		stop = btop / 6;

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, btop * sizeof(GLfloat), ball, GL_STATIC_DRAW);//data will change every framebuffer
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		glBindVertexArray(0);

	}
	virtual void draw() {
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, stop);
	}
};
class SHOWPOINTS {
	float rx, ry, rz;
	GLFWwindow *window;
	GLuint VAO, VBO, VBOVERTICESNUM, IVBO;
	Shader pointsShader;

	glm::vec3 calc(int i, int j, int div) {
		//[0,12)
		float u = 1.0f * i * 3.14159f * 2 / div;
		float v = 1.0f * j * 3.14159f / div - 1.570796f;
		return glm::normalize(glm::vec3(cos(u), tan(v), -sin(u)));
	}
public:
	SHOWPOINTS(int MAX_INSTANCE_SIZE) {
		printf("SHOW init: %d\n", MAX_INSTANCE_SIZE);
		//data[0~2](x0,y0,z0)
		//x,y,z在[-1,1]范围内

		//场景布置：站在(rx,ry,rz)面向(0,0,0),rx ry rz从see.in读取 ry不等于0！
		FILE *sp;
		fopen_s(&sp, "see.in", "r");
		fscanf_s(sp, "%f%f%f", &rx, &ry, &rz);

		glm::vec3 cameraPos = glm::vec3(rx, ry, rz);
		glm::vec3 cameraFront = glm::normalize(-cameraPos);
		glm::vec3 cameraUp;
		if (fabs(ry) <= 0.001f)	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
		else					cameraUp = glm::normalize(glm::vec3(-rx, (rx*rx + rz*rz) / ry, -rz));
		glm::mat4 viewMat = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		glm::mat4 persMat = glm::perspective(45.0f, 1.0f*SCREEN_WIDTH / SCREEN_HEIGHT, 0.1f, 100.0f);


		window = glfwStart(SCREEN_WIDTH, SCREEN_HEIGHT, "SPH simulation");

		pointsShader.mkShader("glShader/shader.vert", NULL, "glShader/shader.frag");
		pointsShader.Use();
		glUniformMatrix4fv(pointsShader.sUni("viewMat"), 1, GL_FALSE, &viewMat[0][0]);
		glUniformMatrix4fv(pointsShader.sUni("persMat"), 1, GL_FALSE, &persMat[0][0]);

		/*glm::vec4 shit(2, 2, 1, 1);
		shit = persMat * viewMat * shit;
		printf("%f,%f,%f,%f\n",shit[0],shit[1],shit[2],shit[3]);
		system("pause");*/

		int div = 20;
		GLfloat *ball = new GLfloat[(div - 1)*(div - 1) * 2 * 3 * 3];
		int btop = -1;
		for (int i = 0; i < div - 1; ++i)
			for (int j = 0; j < div - 1; ++j) {
				//(i,j)->(i+1,j)->(i+1,j+1)
				glm::vec3 sb;
				sb = calc(i, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				//(i,j)->(i+1,j+1)->(i,j+1)
				sb = calc(i, j, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i + 1, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
				sb = calc(i, j + 1, div);
				ball[++btop] = sb.x;
				ball[++btop] = sb.y;
				ball[++btop] = sb.z;
			}
		VBOVERTICESNUM = (div - 1)*(div - 1) * 2 * 3;

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, btop * sizeof(GLfloat), ball, GL_STATIC_DRAW);//data will change every framebuffer
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glGenBuffers(1, &IVBO);
		glBindBuffer(GL_ARRAY_BUFFER, IVBO);
		glBufferData(GL_ARRAY_BUFFER, MAX_INSTANCE_SIZE * 3 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribDivisor(1, 1);//构建实例化数组
		glBindVertexArray(0);

		delete[] ball;
	}
	void draw(int n, float *data, float particleSize) {
		glfwPollEvents();
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		GLfloat *dat = new GLfloat[3 * n];
		for (int i = 0; i < n; ++i) {
			dat[3 * i + 0] = data[3 * i];
			dat[3 * i + 1] = data[3 * i + 1];
			dat[3 * i + 2] = data[3 * i + 2];
		}
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, IVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, n * 3 * sizeof(GLfloat), dat);
		delete[] dat;

		pointsShader.Use();

		glUniform1f(pointsShader.sUni("particleSize"), particleSize);
		glUniform3f(pointsShader.sUni("lightDirec"), 0, 1, 0);

		glDrawArraysInstanced(GL_TRIANGLES, 0, VBOVERTICESNUM, n);
		glfwSwapBuffers(window);
		GLuint err = glGetError();
		if (err)
			std::cout << "Error: " << err << std::endl;
	}
	~SHOWPOINTS() {
		glfwTerminate();
	}
};
float randf(float l, float r) {
	return (rand() % 30000)*1.0f / 30000 * (r - l) + l;
}

/*
水表面模型的构成：
点列(点的坐标+法矢量，6个GLfloat)
三角片列，用点列的编号来代替点，每三个编号表示一个三角片

则三角片中一个点的法矢量由三顶点差值得到。
*/
class SHOWMODEL {
	float rx, ry, rz;
	GLFWwindow *window;
	GLuint VAO, VBO, EBO;
	SkyBox mybox;
	Shader modelShader;
	glm::mat4 viewMat, persMat, viewMatForSkybox, persMatForSkybox;

public:
	SHOWMODEL(int MAX_VBO_SIZE, int MAX_EBO_SIZE = 0) {
		//输入MAX_VBO_SIZE，即 6 * 最多顶点数目
		//MAX_EBO_SIZE，即 3 * 最多三角片数目

		//see.in::
		//视点坐标
		//光源的方向
		//光的颜色
		//data[0~2](x0,y0,z0)
		//x,y,z在[-1,1]范围内

		//场景布置：站在(rx,ry,rz)面向(0,0,0),rx ry rz从see.in读取 ry不等于0！
		FILE *sp;
		fopen_s(&sp, "see.in", "r");
		fscanf_s(sp, "%f%f%f", &rx, &ry, &rz);
		glm::vec3 lightDirec, lightColor;
		fscanf_s(sp, "%f%f%f", &lightDirec[0], &lightDirec[1], &lightDirec[2]);
		fscanf_s(sp, "%f%f%f", &lightColor[0], &lightColor[1], &lightColor[2]);

		//生成变换矩阵
		glm::vec3 cameraPos = glm::vec3(rx, ry, rz);
		glm::vec3 cameraFront = glm::normalize(-cameraPos);
		glm::vec3 cameraUp;
		if (fabs(ry) <= 0.001f)	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
		else					cameraUp = glm::normalize(glm::vec3(-rx, (rx*rx + rz*rz) / ry, -rz));
		viewMat = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		persMat = glm::perspective(45.0f, 1.0f*SCREEN_WIDTH / SCREEN_HEIGHT, 0.1f, 100.0f);
		viewMatForSkybox = glm::lookAt(glm::vec3(0), cameraFront, cameraUp);
		persMatForSkybox = persMat;

		//创建窗口，准备渲染
		window = glfwStart(SCREEN_WIDTH, SCREEN_HEIGHT, "SPH simulation");
		//天空盒
		mybox.make();
		//流体shader
		//modelShader.mkShader("glShader/model.vert", "glShader/model.geom", "glShader/model.frag");
		modelShader.mkShader("glShader/model1.vert", NULL, "glShader/model1.frag");
		modelShader.Use();
		glUniformMatrix4fv(modelShader.sUni("viewMat"), 1, GL_FALSE, &viewMat[0][0]);
		glUniformMatrix4fv(modelShader.sUni("persMat"), 1, GL_FALSE, &persMat[0][0]);
		glUniform3f(modelShader.sUni("EyePos"), rx, ry, rz);
		glUniform3f(modelShader.sUni("LightDirec"), lightDirec.x, lightDirec.y, lightDirec.z);
		glUniform3f(modelShader.sUni("LightColor"), lightColor.x, lightColor.y, lightColor.z);
		//流体model
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, MAX_VBO_SIZE, NULL, GL_STREAM_DRAW);//data will change every framebuffer
		if (MAX_EBO_SIZE != 0) {
			glGenBuffers(1, &EBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_EBO_SIZE, NULL, GL_STREAM_DRAW);
		}
		else
			EBO = 0;
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		glBindVertexArray(0);

	}
	void draw(int verticesNum, GLfloat *vertices, int elementsNum = 0, GLuint *elements = nullptr) {
		//verticesNum：顶点数目
		//vertices：顶点信息(x,y,z)表示坐标
		//elementsNum：3*三角片数目
		//elements：三角片顶点列，每三个GLuint a, b, c 表示由第a,b,c号顶点构成的三角片

		glfwPollEvents();
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		mybox.Draw(viewMatForSkybox, persMatForSkybox);

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, verticesNum * 6 * sizeof(GLfloat), vertices);
		if (elements != nullptr)
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, elementsNum * sizeof(GLuint), elements);

		modelShader.Use();
		glUniformMatrix4fv(modelShader.sUni("viewMat"), 1, GL_FALSE, &viewMat[0][0]);
		glUniformMatrix4fv(modelShader.sUni("persMat"), 1, GL_FALSE, &persMat[0][0]);
		glUniform1i(modelShader.sUni("skybox"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, mybox.cubemapTexture);

		//画线框
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		if (elements != nullptr)
			glDrawElements(GL_TRIANGLES, elementsNum, GL_UNSIGNED_INT, 0);
		else
			glDrawArrays(GL_TRIANGLES, 0, verticesNum);

		glfwSwapBuffers(window);
		GLuint err = glGetError();
		if (err)
			std::cout << "Error: " << err << std::endl;
		return;
	}
	~SHOWMODEL() {
		glfwTerminate();
	}
};
class TEST {
	//代码测试板块
public:
	static void try0(int type) {
		CUDA_SET::start();
		fuck::initializeParticles(type);
		SHOWPOINTS sb(fuck::particleCount * 3 * sizeof(GLfloat));
		int partices = fuck::particleCount;
		float *huaji = new float[partices * 3];
		//_sleep(15000); 
		while (1) {
			for (int i = 0; i < partices; ++i) {
				huaji[3 * i + 0] = fuck::particles[i].position.h[0];
				huaji[3 * i + 1] = fuck::particles[i].position.h[1];
				huaji[3 * i + 2] = fuck::particles[i].position.h[2];
			}
			sb.draw(partices, huaji, fuck::particleLength);
			puts("haha");
			fuck::CUDA_computeFrame();
		}
		delete[] huaji;
		fuck::endParticles();
		CUDA_SET::over();
	}
	static void try1() {
		//此函数生成奇怪的水面，
		//并用来检测SHOWMODEL块的展示效果。

		int n = 100;//[-1,1]100等分
		GLfloat **high = new GLfloat *[n];
		for (int i = 0; i < n; ++i)
			high[i] = new GLfloat[n];

		SHOWMODEL show(8 * n*n * sizeof(GLfloat), 8 * n*n * sizeof(GLuint));

		glm::vec3 *vertices = new glm::vec3[n*n * 2];
		GLfloat *fvertices = new GLfloat[n*n * 6];
		GLuint *indices = new GLuint[(n - 1)*(n - 1) * 3 * 2];

		float squLen = 2.0f / n;
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j)
				high[i][j] = randf(0.0, squLen / 20);
		while (1) {
			//随机生成高度场
			for (int i = 0; i < n; ++i)
				for (int j = 0; j < n; ++j)
					high[i][j] += randf(-squLen / 100, squLen / 100);

			//生成对应顶点列，索引列
			for (int i = 0; i < n; ++i)
				for (int j = 0; j < n; ++j) {
					int num = i * n + j;
					vertices[2 * num] = glm::vec3(2.0f * i / n - 1, high[i][j], 2.0f * j / n - 1);
					vertices[2 * num + 1] = glm::vec3(0, 0, 0);
				}
			for (int i = 0; i < n - 1; ++i)
				for (int j = 0; j < n - 1; ++j) {
					int num = i * (n - 1) + j;
					glm::vec3 Normal;
					GLuint a, b, c;

					//(i+1,j)->(i,j)->(i,j+1)
					a = (i + 1) * n + j;
					b = i * n + j;
					c = i * n + (j + 1);
					indices[6 * num + 0] = a;
					indices[6 * num + 1] = b;
					indices[6 * num + 2] = c;
					Normal = glm::normalize(glm::cross(vertices[2 * b] - vertices[2 * a], vertices[2 * c] - vertices[2 * a]));
					vertices[2 * a + 1] += Normal;
					vertices[2 * b + 1] += Normal;
					vertices[2 * c + 1] += Normal;

					//(i,j+1)->(i+1,j+1)->(i+1,j)
					a = i * n + (j + 1);
					b = (i + 1) * n + (j + 1);
					c = (i + 1) * n + j;
					indices[6 * num + 3] = a;
					indices[6 * num + 4] = b;
					indices[6 * num + 5] = c;
					Normal = glm::normalize(glm::cross(vertices[2 * b] - vertices[2 * a], vertices[2 * c] - vertices[2 * a]));
					vertices[2 * a + 1] += Normal;
					vertices[2 * b + 1] += Normal;
					vertices[2 * c + 1] += Normal;

					//printf("%d %d %d\n", a, b, c);
				}

			//将法矢量平均
			for (int i = 0; i < n * n; ++i)
				vertices[2 * i + 1] = glm::normalize(vertices[2 * i + 1]);
			for (int i = 0; i < n * n; ++i) {
				fvertices[6 * i + 0] = vertices[2 * i].x;
				fvertices[6 * i + 1] = vertices[2 * i].y;
				fvertices[6 * i + 2] = vertices[2 * i].z;
				fvertices[6 * i + 3] = vertices[2 * i + 1].x;
				fvertices[6 * i + 4] = vertices[2 * i + 1].y;
				fvertices[6 * i + 5] = vertices[2 * i + 1].z;
			}
			show.draw(n*n, fvertices, (n - 1)*(n - 1) * 6, indices);
		}

		delete[] vertices;
		delete[] fvertices;
		delete[] indices;
		for (int i = 0; i < n; ++i)
			delete[] high[i];
		delete[] high;
	}
	static void try2() {
		CUDA_SET::start();
		int n = 1;
		float ha[3] = { 0,0,0 };
		SHOWMODEL show(200 * 200 * 200 * 6);
		while (1) {
			int huafirst;
			float *huasecond = CUDA_MARCHING_CUBE::run(huafirst, 1, ha, 0.8, 0.01, -1, 1, -1, 1, -1, 1);
			printf("Triangles: %d\n", huafirst);
			show.draw(huafirst * 3, huasecond);
		}
		CUDA_SET::over();
	}
	static void run(int type) {
		CUDA_SET::start();
		fuck::initializeParticles(type==3?1:2);
		float wat_r = 0.021, wat_div = 0.02;
		if (type == 3)
			wat_r = 0.042;
		if (type == 5)
			wat_div = 0.01;
		SHOWMODEL show(MC_MAX_TRIANGLES * 18 * sizeof(GLfloat));
		int partices = fuck::particleCount;
		float *huaji = new float[partices * 3];
		//_sleep(15000);
		while (1) {
			for (int i = 0; i < partices; ++i) {
				huaji[3 * i + 0] = fuck::particles[i].position.h[0];
				huaji[3 * i + 1] = fuck::particles[i].position.h[1];
				huaji[3 * i + 2] = fuck::particles[i].position.h[2];
			}
			int huafirst;
			float *huasecond = CUDA_MARCHING_CUBE::run(huafirst, partices, huaji, wat_r, wat_div, -1, 1, -1, 1, -1, 1);
			printf("Triangles: %d\n", huafirst);
			show.draw(huafirst * 3, huasecond);
			fuck::CUDA_computeFrame();
		}
		delete[] huaji;
		fuck::endParticles();
		CUDA_SET::over();
	}
};
int main() {
	printf("input\n1.5w粒子\n2.10w粒子\n3.低精度流体\n4.中精度流体\n5.高精度流体\n");
	int type; 
	scanf("%d", &type);
	switch(type) {
	case 1: {
		TEST::try0(1);
		break;
	}
	case 2: {
		TEST::try0(2);
		break;
	}
	case 3: {
		TEST::run(3);
		break;
	}
	case 4: {
		TEST::run(4);
		break;
	}
	case 5: {
		TEST::run(5);
		break;
	}
	default: {
		break;
	}
	}
	return 0;
}