#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 offset;

uniform mat4 viewMat;
uniform mat4 persMat;
uniform float particleSize;

out vec3 Normal;
out vec3 Pos;
void main() {
	gl_Position = persMat * viewMat * vec4(pos*particleSize + offset, 1);
	Normal = pos;
	Pos = offset * 0.5 + 1;
}