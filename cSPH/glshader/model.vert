#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
out vec3 position;
out vec3 nnormal;
void main() {
	gl_Position = vec4(pos, 1);
	position = pos;
	nnormal = normal;
}