#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

uniform mat4 viewMat;
uniform mat4 persMat;
in vec3 position[];
in vec3 nnormal[];
out vec3 Position;
out vec3 Normal;

void main() {
	vec3 tnormal = normalize(cross(normalize(position[1]-position[0]), normalize(position[2]-position[0])));
	Normal = tnormal;
	Position = position[0];
	gl_Position = persMat * viewMat * gl_in[0].gl_Position;
	EmitVertex();
	Position = position[1];
	gl_Position = persMat * viewMat * gl_in[1].gl_Position;
	EmitVertex();
	Position = position[2];
	gl_Position = persMat * viewMat * gl_in[2].gl_Position;
	EmitVertex();
    EndPrimitive();
}