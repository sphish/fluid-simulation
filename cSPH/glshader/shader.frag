#version 330 core
in vec3 Normal;
in vec3 Pos;
out vec4 col;

uniform vec3 lightDirec;
void main() {
	float hh = pow(1.5*Pos.y,2);
	col = dot(normalize(lightDirec+Normal), Normal) * hh * vec4(0,0.5,1,1);
}