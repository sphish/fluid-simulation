#version 330 core
layout(location = 0) in vec3 pos;

uniform mat4 viewMat;
uniform mat4 persMat;

out vec3 direc;
void main() {
	vec4 hh = persMat * viewMat * vec4(pos, 1);
    gl_Position = vec4(hh.x, hh.y, hh.z*0.9999, hh.z);
    direc = pos;
}