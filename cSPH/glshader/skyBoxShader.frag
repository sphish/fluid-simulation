#version 330 core
in vec3 direc;
uniform samplerCube skybox;

out vec4 color;
void main() {
	color = texture(skybox, direc);
	//color = vec4(direc/2+0.5, 1);
}