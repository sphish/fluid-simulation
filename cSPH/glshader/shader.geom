#version 330 core
layout (points) in;
layout (points, max_vertices = 1) out;

uniform mat4 viewMat;
uniform mat4 persMat;
in vec3 Pos[];
out vec3 PPos;
vec3 calc(int i, int j) {
    //[0,12)
    float u = 1.0f * i * 3.14159f / 2;
    float v = 1.0f * j * 3.14159f / 4 - 1.570796f;
    return normalize(vec3(cos(u), tan(v), -sin(u)));
}
void main() {
    PPos = Pos[0];
    gl_Position = persMat * viewMat * gl_in[0].gl_Position * particleSize;
    EmitVertex();
    EndPrimitive();
}