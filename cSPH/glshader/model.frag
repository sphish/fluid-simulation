//当前采用直接反射+直接折射混合模式，忽略了灯光。
#version 330 core
in vec3 Position;
in vec3 Normal;
uniform vec3 EyePos;
uniform vec3 LightDirec;//到光照源的方向，假定都是平行光，限定为单位矢量
uniform vec3 LightColor;

uniform samplerCube skybox;//暂且只用天空盒表示去除水面之外的东西

out vec4 col;

//finalColor = reflectionCoefficient * reflectedColor + (1 - reflectionCoefficient ) * refractedColor

vec3 calcReflect(vec3 normal, vec3 toeye) {
	vec3 shit = dot(normal, toeye) * 2 * normal;
	return shit - toeye;
}
const float FresnelPower = 6.0;//菲涅尔指数，决定反射比例的收敛速度
void main() {
	vec3 toEye = normalize(EyePos - Position);


	float reflectionCoefficient = max(0, min(1, 0.0 + 1 * pow(1 - dot(toEye,Normal), FresnelPower)));//反射的比利

	vec3 reflect = calcReflect(Normal, toEye);
	vec3 reflectCol = texture(skybox, reflect).xyz;

    float ratio = 1.00 / 1.33;
    vec3 refract = refract(-toEye, Normal, ratio);
	vec3 refractCol = texture(skybox, refract).xyz;
	col = vec4(reflectCol * reflectionCoefficient + refractCol * (1.0 - reflectionCoefficient), 1);

	//col = vec4(1,1,1,1);
	//test for Marching Cube;
	//col = vec4(Position.xyz/2+0.5,1);
	//col = vec4(Normal*2+0.5, 1);
}