#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
out vec3 Position;
out vec3 Normal;
uniform mat4 viewMat;
uniform mat4 persMat;
void main() {
	gl_Position = persMat * viewMat * vec4(pos, 1);
	Position = pos;
	Normal = normal;
}